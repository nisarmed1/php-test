<?php namespace PHPTest\Q2_8;

/*
 * S3Client stub, should be created using PHPUnit, this class is for demonstration only
 */
class S3Client
{
    public function getIterator($operation, $params, $options) : \Iterator
    {
        return new class implements \Iterator {
            private $position = 0;
            private $array = array(
                ["Key" => "firstfile.xml"],
                ["Key" => "secondfile.xml"],
            );  
        
            public function __construct() {
                $this->position = 0;
            }
        
            public function rewind() {
                $this->position = 0;
            }
        
            public function current() {
                return $this->array[$this->position];
            }
        
            public function key() {
                return $this->position;
            }
        
            public function next() {
                ++$this->position;
            }
        
            public function valid() {
                return isset($this->array[$this->position]);
            }
        };
        
    }
}