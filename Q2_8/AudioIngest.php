<?php namespace PHPTest\Q2_8;

class AudioIngest extends BaseIngest {

    private $s3_client;
    
    /*
     * Use constructor injection to inject S3Client object
     */
    public function __construct(S3Client $s3_client)
    {
        $this->s3_client = $s3_client;
    }
 
    public function getTotalPodcastFiles()
    {
        $xmlFilesFromS3Iterator = $this->s3_client->getIterator(
            'ListObjects',
            array('Bucket' => 'testbucket', 'Prefix' => '/test'),
            array('limit' => 50)
        );
 
        $xmlFilesFromS3Array = iterator_to_array($xmlFilesFromS3Iterator);
 
        $number_of_podcast_xml_files = 0;
        foreach ($xmlFilesFromS3Array as $xmlFile) {
            if (pathinfo($xmlFile['Key'], PATHINFO_EXTENSION) == 'xml') {
                $number_of_podcast_xml_files++;
            }
        }
        return $number_of_podcast_xml_files;
    }
 }
 