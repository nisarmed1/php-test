<?php namespace PHPTest\Q2_8;

abstract class BaseIngest
{
    abstract public function getTotalPodcastFiles();
}