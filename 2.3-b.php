<?php 

function evaluate_poker_hand($hand, &$is_straight, &$is_flush)
{
    $ranks = [2,3,4,5,6,7,8,9,10,'j','q','k','a'];
    $ranks = array_map(function($card) use($ranks) {
        return array_search(substr($card, 0, strlen($card)-1), $ranks);
    }, $hand);
    $suits = array_map(function($card) {
        return $card[strlen($card)-1];
    }, $hand);
    rsort($ranks);
    if($ranks[0] == 12 && $ranks[1] == 3)
        $ranks = [3, 2, 1, 0, -1];
    $is_straight = $ranks[0] - $ranks[4] === 4;
    $is_flush = count(array_unique($suits)) === 1;
}

// Can be tested with command `./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/PokerHandEvaluationTest.php` in project root