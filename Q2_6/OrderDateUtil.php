<?php namespace PHPTest\Q2_6;

class OrderDateUtil
{
    private $orderDate;
    public function __construct($date)
    {
        $this->orderDate = $date;
    }
    public static function createFromDate($date)
    {
        return new OrderDateUtil($date);
    }
    static function getNextWorkingDate($date) {
        $date = clone $date;
        $year = date('Y', $date->getTimestamp());
        $publicHolidays = self::getPublicHolidaysForYear($year);
        do {
            $dayOfYear = date('z', $date->getTimestamp());
            if(in_array($dayOfYear, $publicHolidays)) {
                $date->modify("+1 day 9am");
                continue;
            }
            $dayOfWeek = date('N', $date->getTimestamp());
            if($dayOfWeek >= 6) {
                $d = 8 - $dayOfWeek;
                $date->modify("+{$d} day 9am");
                continue;
            }
            $hour = date('G', $date->getTimestamp());
            if($hour >= 16) {
                $date->modify("+1 day 9am");
                continue;
            }
            break;
        } while(true);
        return $date;
    }
    public function getDispatchDate() {
        $orderDate = $this->orderDate;
        $dispatchDate = \DateTime::createFromFormat('Y-m-d H-i-s', $orderDate);
        if(!$dispatchDate) {
            throw new \Exception("Invalid date format $orderDate");
        }
        $dispatchDate = self::getNextWorkingDate($dispatchDate);
        return $dispatchDate->format('Y-m-d H-i-s');
    }

    public function getDeliveryDate($days=3) {
        $dispatchDate = $this->getDispatchDate();
        $deliveryDate = \DateTime::createFromFormat('Y-m-d H-i-s', $dispatchDate);
        if(!$deliveryDate) {
            throw new \Exception("Invalid date format");
        }
        while(--$days > 0)
        {
            $deliveryDate->modify("+1 day");
            $deliveryDate = self::getNextWorkingDate($deliveryDate);
        }
        return $deliveryDate->format('Y-m-d H-i-s');
    }

    static function getPublicHolidaysForYear($year) {
        // • New Year's Day : 1st January
        // • Australia Day : 26th January
        // • Good Friday (Easter) : Variable date
        // • Easter Monday : Variable date
        // • Anzac Day : 25th April
        // • Queen's Birthday : Second Monday in June
        // • Labour Day : First Monday in October
        // • Christmas Day : 25th December
        // • Boxing Day : 26th December
        $holidays = [
            strtotime("$year-01-01"),
            strtotime("$year-01-26"),
            strtotime("last friday", easter_date($year)),
            strtotime("next monday", easter_date($year)),
            strtotime("second monday $year-06"),
            strtotime("first monday $year-10"),
            strtotime("$year-12-25"),
            strtotime("$year-12-26")
        ];
        // if any of the holidays fall on a weekend, it should be rolled over to next monday except for Anzac day
        return array_merge(array_map(function($holiday) {
            return (7-date('N', $holiday) <= 1) ? date('z', $strtotime("next monday", $holiday)) : date('z', $holiday);
        }, $holidays), [date('z', strtotime("$year-04-25"))]);
    }
}