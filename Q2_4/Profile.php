<?php namespace PHPTest\Q2_4;

class Profile {
    private $name;
    private $phoneNumber;
    private $email;

    public function setName($name) {
        $this->name = $name;
        return $this;
    }
    public function getName() {
        return $this->name;
    }
    public function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }
    public function getPhoneNumber() {
        return $this->phoneNumber;
    }
    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }
    public function getEmail() {
        return $this->email;
    }
}

# this class can be tested with command `./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/FluentInterfaceTest.php` in project root