
A way to add filter is to create a Decorator class to add functionality to original class.

The filter can be added to the callback anonymous function passed as first argument of the original class

<?php

class DecoratorController
{
    private $controller;
    public function __construct(SomeController $controller)
    {
        $this->controller = $controller;
    }
    public function listAction($type)
    {
        $this->controller->listAction(function ($qb) {
            $qb->field('type')->equals($type);
        });

    }
}
