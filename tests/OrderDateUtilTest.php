<?php
use PHPUnit\Framework\TestCase;
use PHPTest\Q2_6\OrderDateUtil;

class OrderDateUtilTest extends TestCase
{
    public function testNextWorkingDate()
    {
        $this->assertEquals('2018-01-02 09-00-00', OrderDateUtil::getNextWorkingDate(
            \DateTime::createFromFormat('Y-m-d H-i-s', '2018-01-01 03-00-00')
            )->format('Y-m-d H-i-s'));
        $this->assertEquals('2018-01-29 09-00-00', OrderDateUtil::getNextWorkingDate(
            \DateTime::createFromFormat('Y-m-d H-i-s', '2018-01-26 09-00-00')
            )->format('Y-m-d H-i-s'));
        $this->assertEquals('2018-01-29 09-00-00', OrderDateUtil::getNextWorkingDate(
            \DateTime::createFromFormat('Y-m-d H-i-s', '2018-01-25 17-00-00')
            )->format('Y-m-d H-i-s'));
    }
    public function testDispatchDate()
    {
        $this->assertEquals('2018-01-02 09-00-00', OrderDateUtil::createFromDate('2018-01-01 03-00-00')->getDispatchDate());
        $this->assertEquals('2018-05-01 03-00-00', OrderDateUtil::createFromDate('2018-05-01 03-00-00')->getDispatchDate());
        $this->assertEquals('2018-05-02 09-00-00', OrderDateUtil::createFromDate('2018-05-01 17-00-00')->getDispatchDate());
        $this->assertEquals('2018-04-26 09-00-00', OrderDateUtil::createFromDate('2018-04-24 17-00-00')->getDispatchDate());
    }
    public function testDeliveryDate()
    {
        $this->assertEquals('2018-01-31 09-00-00', OrderDateUtil::createFromDate('2018-01-26 12-00-00')->getDeliveryDate());
        $this->assertEquals('2018-05-04 09-00-00', OrderDateUtil::createFromDate('2018-05-01 18-00-00')->getDeliveryDate());
        $this->assertEquals('2018-06-13 09-00-00', OrderDateUtil::createFromDate('2018-06-08 12-00-00')->getDeliveryDate());
        $this->assertEquals('2018-04-05 09-00-00', OrderDateUtil::createFromDate('2018-04-01 12-00-00')->getDeliveryDate());
    }
}