<?php
use PHPUnit\Framework\TestCase;
use PHPTest\Q2_4\Profile;

class FluentInterfaceTest extends TestCase
{
    public function testName()
    {
        $profile = new Profile();

        $this->assertEquals('Nisar', $profile->setName("Nisar")->getName());
    }
}