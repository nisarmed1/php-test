<?php
require_once("2.5.php");

use PHPUnit\Framework\TestCase;

class BuySellStockTest extends TestCase
{
    public function testTotalProfit()
    {
        $transactions = buy_sell_stock(array(360, 255, 260, 230, 150, 100, 135, 265, 750, 460, 600));
        $this->assertEquals(795, $transactions['total_profit']);

        $transactions = buy_sell_stock(array(300, 250, 260, 310, 215, 280));
        $this->assertEquals(125, $transactions['total_profit']);
    }
}