<?php
use PHPUnit\Framework\TestCase;
use PHPTest\Q2_8\AudioIngest;
use PHPTest\Q2_8\S3Client;

class AudioIngestTest extends TestCase
{
    public function testGetTotalPodcastFiles()
    {
        $stub = new S3Client();

        $ingest = new AudioIngest($stub);
        $this->assertEquals(2, $ingest->getTotalPodcastFiles());
    }
}