<?php
require_once("2.1.php");

use PHPUnit\Framework\TestCase;

class CipherTest extends TestCase
{
    public function testCipher()
    {
        $this->assertEquals('916192113', cipher('Ipsum'));
        $this->assertEquals('121518513', cipher('Lorem'));
    }
}