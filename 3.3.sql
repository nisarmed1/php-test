--Use left outer join to get the results

SELECT DISTINCT t1.id, t1.name, t1.date, t2.column_x
FROM table_1 t1 LEFT JOIN table_2 t2 ON t1.date=t2.date
WHERE t1.category_id = 10
ORDER BY t1.date
LIMIT 10