<?php

/* This function has a time complexity of O(N). This this function using following command on root of this project
 *
 * ./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/BuySellStockTest.php 
 * 
 * composer install is required to run before running the unit test
 */


function buy_sell_stock($price_history)
{
    $selling = false;
    $tid = 0;
    for($i=1;$i<count($price_history);$i++)
    {
        $price = $price_history[$i];
        $last_price = $price_history[$i-1];
        if($last_price < $price && $selling === false)
        {
            $transactions[$tid]['bought'] = [
                'day' => $i, 'value' => $last_price
            ];
            $selling = true;
        }

        if($last_price > $price && $selling === true)
        {
            $transactions[$tid]['sold'] = ['day' => $i, 'value' => $last_price];
            $transactions[$tid]['profit'] = $transactions[$tid]['sold']['value'] - $transactions[$tid]['bought']['value'];
            $tid++;
            $selling = false;
        }

        if($i == count($price_history)-1 && $selling === true)
        {
            $transactions[$tid]['sold'] = ['day' => $i+1, 'value' => $price];
            $transactions[$tid]['profit'] = $transactions[$tid]['sold']['value'] - $transactions[$tid]['bought']['value'];
        }
    }
    $transactions['total_profit'] = array_reduce($transactions, function($total, $transaction){
        $total += $transaction['profit'];
        return $total;
    }, 0);
    return $transactions;

}
