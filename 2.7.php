<?php
/*
 * The main thing to fix here is this code is vulnarable to SQL Injection
 * by using prepared statements, it can be avoided
 * 
 * second is we should throw exception instead of calling die so that error can be handled by code calling this function
 * 
 * third is when display data on the webpage, one should convert special HTML charaters to HTML entities
 * 
 * fourth, it should also display a message when no records are found
 * 
 */

// Connect to the database.
if(!mysql_connect("localhost", 'admin', "password")) {
    throw new Exception(mysql_error());
}
if(!mysql_select_db("my_database")) {
    throw new Exception(mysql_error());
}
// Set an INTEGER variable to be used within the database query.
$example_integer = 123;

// Retrieve ONLY the column 'example_column' from 'example_table'.

/* create a prepared statement */
if ($stmt = $mysqli->prepare('SELECT * FROM example_table WHERE "example_column"=? AND "example_column" NOT IN (?)')) {

    /* bind parameters for markers */
    $stmt->bind_param("si", $_REQUEST['parameter'], $example_integer);

    /* execute query */
    $stmt->execute();

    $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);

    // Loop through the returned record(s) from the database query and output all data for each row.
    foreach ($result as $row){ 
        print('<pre>' . htmlspecialchars($row));
    } 
  
    /* close statement */
    $stmt->close();
}
else {
    throw new Exception(mysql_error());
}

