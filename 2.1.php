<?php

function cipher($text) {
    $cipher = '';
    for($i=0;$i<mb_strlen($text);$i++) {
        $character = mb_strtolower(mb_substr($text, $i, 1));
        $cipher = $cipher . (ord($character) % 96);
    }
    return $cipher;
}

print cipher('Ipsum') . "\n";

# output is 916192113
# can be futher tested with command `./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/CipherTest.php` in project root