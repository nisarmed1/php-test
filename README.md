# PHP Test

## Solution folder structure

The solution to each problems listed in the tech_assessment_questions.txt file is provided in its own file or a folder (where namespace is required).

 Following questions have a corresponding unit test
 
 * 2.1
 * 2.3-b
 * 2.4
 * 2.5
 * 2.6
 * 2.8

## PHPUnit

in order to run phpunit tests please install in root folder

`composer install`

then

`./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/`

